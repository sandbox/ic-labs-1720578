<?php

/*
 * @file : Pages callbacks and form builder functions for linkedin module
 */

/*
 * Callback for linkedin related user settings at user/%/edit/linkedin
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function linkedin_user_settings($account) {
  $output = array();
  $check = linkedin_get_profile_fields($account->uid, array('first-name', 'last-name', 'public-profile-url'), TRUE);
  if (isset($check['error-code'])) {
    //$output .= t('You must first authorize LinkedIn integration to use related features.');
    $output['form'] = drupal_get_form('linkedin_user_enable_form', $account->uid);
  }
  else {
    //$output .= t('Your account is associated with @name !public_profile.', array('@name' => $check['first-name'] . ' ' . $check['last-name'], '!public_profile' => l($check['public-profile-url'], $check['public-profile-url'])));
    $output['form'] = drupal_get_form('linkedin_user_settings_form', array('account' => $account, 'ldaccount' => $check));
  }
  return $output;
}

/*
 * Take users to linkedin auth page
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function linkedin_user_enable_form($form, $form_state, $uid) {
  $form['#user'] = $uid;
  $form['#action'] = url('linkedin/token/' . $uid);
  $form['linkedin-set'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('You must first authorize LinkedIn integration to use related features.'),
  );
  $form['linkedin-set']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go LinkedIn'),
    '#suffix' => '<p class="description">' . t('You will be taken to LinkedIn website in order to complete the process.') . '</p>',
  );
  return $form;
}

/*
 * form for individual user settings
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function linkedin_user_settings_form($form, &$form_state, $accounts) {
  $form['linkedin'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('Your account is associated with @name !public_profile.', array('@name' => $accounts['ldaccount']['first-name'] . ' ' . $accounts['ldaccount']['last-name'], '!public_profile' => l($accounts['ldaccount']['public-profile-url'], $accounts['ldaccount']['public-profile-url']))),
  );
  // Just fetch forms from submodules.
  $form['linkedin']['elements'] = module_invoke_all('linkedin_user_settings_page', $accounts['account']);
  // We will need the account at submit
  $form['#account'] = $accounts['account'];

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  $form['linkedin']['reset'] = array(
    '#type' => 'submit',
    '#description' => t('Click here to unlink your LinkedIn account.'),
    '#value' => t('Unlink'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function linkedin_user_settings_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];

  if ($op == $form['linkedin']['reset']['#value']) {
    $data = array();
    foreach ($form_state['values'] as $key => $val) {
      if (substr($key, 0, 8) == 'linkedin') {
        $data[$key] = NULL;
      }
    }
    user_save($form['#account'], array('data' => $data), 'linkedin');
    db_delete('linkedin_token')
    ->condition('uid', $form['#account']->uid)
    ->execute();
    drupal_set_message(t('LinkedIn preferences have been reset'));
  }
  else {
    $data = array();
    foreach ($form_state['values'] as $key => $val) {
      if (substr($key, 0, 8) == 'linkedin') {
        $data[$key] = $val;
      }
    }
    user_save($form['#account'], array('data' => $data), 'linkedin');
    drupal_set_message(t('LinkedIn preferences have been saved'));
  }
}
