<?php

/**
 * @file
 * Template file for LinkedIn "Positions" items as displayed on user page
 * Copy it to your theme's folder if you want to override it.
 *
 * Be sure to check and comply to  http://developer.linkedin.com/docs/DOC-1091 before tweaking.
 */
$feeds == $variables['feeds'];
//print_r($feeds);
?>
<div class="linkedin-positions"><?php
if(isset($feeds['error']) && $feeds['error'] != '') {
  echo $feeds['error'];
} else {
  ?>
<div class="linkedin_share_post"><?php echo drupal_render(drupal_get_form('share_post')); ?>
<div style="clear: both;"></div>
</div>
  <?php
  foreach($feeds as $feed) {
    ?>
<div class="linkedin_share_msg">
<div class="user_thumb"><a href="<?php echo $feed['profile_link'];?>"
	target="_blank"><img src="<?php echo $feed['user_image'];?>" /></a></div>
<div class="msg_content">
<div class="user_post"><span class="user_name"><a
	href="<?php echo $feed['profile_link'];?>" target="_blank"><?php echo $feed['post_user'];?></a></span>
	<?php if($feed['user_comment'] != '') {?> <span class="user_message"><?php echo $feed['user_comment'];?></span>
	<?php }?></div>
<div class="user_post_content"><?php if ($feed['comment_content_image'] != '') {?>
<div class="post_content_image"><a
	href="<?php echo $feed['comment_content_link'];?>"><img
	src="<?php echo $feed['comment_content_image'];?>" /></a></div>
	<?php }?>
<div class="post_content_body"><span class="content_title"><a
	href="<?php echo $feed['comment_content_link'];?>"><?php echo $feed['comment_content_title'];?></a></span>
<br />
<?php if($feed['comment_content_desc']) {?> <span class="content_body"><?php echo $feed['comment_content_desc'];?></span>
<?php }?></div>
</div>
<div class="clear"></div>
<div class="like_comment_cont"><?php if($feed['is_liked'] == 'true'):?>
<a href="#" title="<?php echo $feed['id'];?>"
	alt="<?php echo $feed['update_likes'];?>" name="unlike">Unlike</a> <?php else:?>
<a href="#" title="<?php echo $feed['id'];?>"
	alt="<?php echo $feed['update_likes'];?>" name="like">Like</a> <?php endif;?>
<span id="like_<?php echo $feed['id'];?>"><?php echo "(".$feed['update_likes'].")";?></span>
&bull; Comments <span id="comments_<?php echo $feed['id'];?>"><?php echo "(".$feed['update_comments_count'].")";?></span>
&bull; <?php echo $feed['comment_time'];?></div>
<?php if($feed['is_liked'] == 'true'):?>
<div id="user_like_<?php echo $feed['id'];?>"
	class="linkedin_post_comment">You Like this</div>
<?php else:?>
<div id="user_like_<?php echo $feed['id'];?>"></div>
<?php endif;?>
<div class="linkedin_stream_comments"
	id="linkedin_stream_comments_<?php echo $feed['id'];?>"><?php if(isset($feed['update_comments']) && $feed['update_comments_count'] > 0) :
	if (isset($feed['update_comments'][0])) :
	foreach ($feed['update_comments'] as $comment) {
	  ?>
<div class="linkedin_post_comment">
<div class="linkedin_comment_user_picture"><img
	src="<?php echo $comment['person']['picture-url']; ?>" /></div>
<div class="linkedin_comment_message"><a
	href="<?php echo $comment['person']['site-standard-profile-request']['url']; ?> "><?php echo $comment['person']['first-name'] . $comment['person']['last-name']; ?>
</a> <span class="linkedin_comment_user_message"><?php echo str_parse_url(check_plain($comment['comment'])); ?>
</span> <?php $time = time_duration(time() - ($comment['timestamp']/1000)).' ago'; ?>
<div class="linkedin_comment_created"><?php echo $time ?></div>
</div>
<div style="clear: both;"></div>
</div>
	  <?php }
	  else:
	  ?>
<div class="linkedin_post_comment">
<div class="linkedin_comment_user_picture"><img
	src="<?php echo isset($feed['update_comments']['person']['picture-url'])?$feed['update_comments']['person']['picture-url']:''; ?>" /></div>
<div class="linkedin_comment_message"><a
	href="<?php echo $feed['update_comments']['person']['site-standard-profile-request']['url']; ?> "><?php echo $feed['update_comments']['person']['first-name'] . $feed['update_comments']['person']['last-name']; ?>
</a> <span class="linkedin_comment_user_message"><?php echo str_parse_url(check_plain($feed['update_comments']['comment'])); ?>
</span> <?php $time = time_duration(time() - ($feed['update_comments']['timestamp']/1000)).' ago'; ?>
<div class="linkedin_comment_created"><?php echo $time ?></div>
</div>
<div style="clear: both;"></div>
</div>
	  <?php
	  endif;
	  endif;?></div>
<div class="linkedin_post_comment">
<div id="loader_<?php echo $feed['id'];?>" style="display: none;"></div>
	  <?php echo drupal_render(drupal_get_form('linkedin_post_comment_form', $feed['id'], $feed['update_comments_count'])); ?>
</div>
</div>
<div class="clear"></div>
</div>
	  <?php
  }
}
?></div>

