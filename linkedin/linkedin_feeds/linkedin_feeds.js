Drupal.linkedin = Drupal.linkedin || {};
Drupal.linkedin.comments_submit = function(event){
	
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
    var comment = this.comment.value; 
    var post_id = this.post_id.value;
    var comment_count = this.comment_count.value;

    var form = this;
    if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	return false;
    }
    jQuery.ajax({
    	url: Drupal.settings.basePath+'linkedin/post/comment',
    	type: "POST",
    	data: {id:post_id,comments:comment},
    	beforeSend: function() {
    		jQuery('#loader_'+post_id).show();
    		jQuery('#loader_'+post_id).append(loaderImg);
    	},
    	success: function(data) {
    		form.comment.value = '';
    		jQuery(data).appendTo(jQuery('#linkedin_stream_comments_'+post_id));
    		jQuery('#comments_'+post_id).html("("+(parseInt(comment_count)+1)+")");
    		jQuery('#loader_'+post_id).hide();
    		jQuery('.loader').remove();
    	}
    });
    event.preventDefault();
    event.stopPropagation();
    return false;
}
Drupal.linkedin.like_handler = function(event){
	var post_id = jQuery(this).attr("title");
	var likevalue = jQuery(this).attr("name");
	var like_count = jQuery(this).attr("alt");
	var link = jQuery(this);
	jQuery.post(Drupal.settings.basePath+'linkedin/post/likes',{id:post_id,value:likevalue},function(data){
		var dataar = jQuery.parseJSON(data);
		if (dataar.error_code) {
			alert(dataar.message);
		} else { 
			if (likevalue == 'like') {
				if (like_count > 0) {
					jQuery('#like_'+post_id).html("("+(parseInt(like_count) + 1)+")");
					link.attr("alt", (parseInt(like_count) + 1));
				} else {
					jQuery('#like_'+post_id).html("(1)");
					link.attr("alt", 1);
				}
				link.html('Unlike');
				link.attr("name", "unlike");
			} else {
				if (like_count > 1) {
					jQuery('#like_'+post_id).html("("+(parseInt(like_count) - 1)+")");
					link.attr("alt", (parseInt(like_count) - 1));
				} else {
					jQuery('#like_'+post_id).html("(0)");
					link.attr("alt", 0);
				}
				link.html('Like');
				link.attr("name", "like");
			}	
			jQuery('#user_like_'+post_id).html("You "+likevalue+" this");
			jQuery('#user_like_'+post_id).addClass('linkedin_post_comment');
		}	
		
	});
	return false;
}
Drupal.linkedin.share_form_submit = function(event){
	event.preventDefault();
    event.stopPropagation();
    jQuery('input[type=submit]', this).attr('disabled', 'disabled');
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
	var comment = this.status.value;
	if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	jQuery('input[type=submit]', this).removeAttr('disabled');
    	return false;
    } else {
    	this.submit();
    	return true;
    }
}
jQuery('.linkedin_share_add_form').bind('submit', Drupal.linkedin.share_form_submit);
jQuery('.linkedin_comment_add_form').bind('submit',Drupal.linkedin.comments_submit);
jQuery('.like_comment_cont a').bind('click',Drupal.linkedin.like_handler);