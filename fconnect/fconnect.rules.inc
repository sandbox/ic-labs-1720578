<?php

/**
 * @file
 * Rules integration for the fconnect module.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function fconnect_rules_event_info() {
  return array(
    'fconnect_user_registered' => array(
      'label' => t('User has connected his Facebook Account'),
      'module' => 'fconnect',
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('granted facebook auth user'),
  ),
  ),
  ),
    'fconnect_user_unregistered' => array(
      'label' => t('User has disconnected his Facebook Account'),
      'module' => 'fconnect',
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('revoked facebook auth user'),
  ),
  ),
  ),
    'fconnect_user_login' => array(
      'label' => t('User has logged in via Facebook'),
      'module' => 'fconnect',
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('logged in via facebook user'),
  ),
  ),
  ),
  );
}

/**
 * @}
 */
