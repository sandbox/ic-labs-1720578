Drupal.fconnect = Drupal.fconnect || {};

Drupal.fconnect.comments_form_submit = function(event){
	
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
    var comment = this.comment.value; 
    var post_id = this.post_id.value;
    var comment_count = this.comment_count.value;

    var form = this;
    if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	return false;
    }
    jQuery.ajax({
    	url: Drupal.settings.basePath+'facebook/post/comments',
    	type: "POST",
    	data: {id:post_id,comments:comment},
    	beforeSend: function() {
    		jQuery('#loader_'+post_id).show();
    		jQuery('#loader_'+post_id).append(loaderImg);
    	},
    	success: function(data) {
    		form.comment.value = '';
    		jQuery(data).appendTo(jQuery('#fb_stream_post_comments_'+post_id));
    		jQuery('#comments_'+post_id).html((parseInt(comment_count)+1)+' comments');
    		jQuery('#loader_'+post_id).hide();
    		jQuery('.loader').remove();
    	}
    });
    event.preventDefault();
    event.stopPropagation();
    return false;
}
Drupal.fconnect.like_click_handler = function(event){
	var post_id = jQuery(this).attr("title");
	var likevalue = jQuery(this).attr("name");
	var like_count = jQuery(this).attr("href");
	var link = jQuery(this);
	jQuery.post(Drupal.settings.basePath+'facebook/post/likes',{id:post_id,value:likevalue},function(data){
		if (likevalue == 'Like') {
			if (like_count > 0) {
				jQuery('#user_like_'+post_id).html("You and other ");
				jQuery('#likes_'+post_id).html((parseInt(like_count))+' likes');
			} else {
				jQuery('#user_like_'+post_id).html("You "+likevalue+" this");
			}
			link.html('Unlike');
			link.attr("name", "Unlike");
		} else {
			jQuery('#user_like_'+post_id).html("You "+likevalue+" this");
			link.html('Like');
			link.attr("name", "Like");
		}	
		
	});
	return false;
}

Drupal.fconnect.status_form_submit = function(event){
	event.preventDefault();
    event.stopPropagation();
    jQuery('input[type=submit]', this).attr('disabled', 'disabled');
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
	var comment = this.status.value;
	if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	jQuery('input[type=submit]', this).removeAttr('disabled');
    	return false;
    } else {
    	jQuery('.fb_post_status').prepend(loaderImg);
    	this.submit();
    	return true;
    }
}
jQuery('.fb_stream_comment_add_form').bind('submit',Drupal.fconnect.comments_form_submit);
jQuery('.fb_stream_status_add_form').bind('submit',Drupal.fconnect.status_form_submit);
jQuery('.like_link a').bind('click',Drupal.fconnect.like_click_handler);