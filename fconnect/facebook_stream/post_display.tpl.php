<?php
global $base_url;
?>
<div class="fb_user_picture"><img
	src="<?php echo $user_info['pic_square'];?> " /></div>
<div class="message_wrapper">
<div class="fb_post_user"><a href="<?php echo $user_info['url']; ?> "><?php echo $user_info['name']; ?></a>
</div>
<div class="fb_post_message"><span class="message"><?php if ($post['message'] != '' ) { echo str_parse_url(check_plain($post['message'])); } else { echo check_plain($post['description']); } ?></span>
</div>
<div style="clear: both;"></div>
<?php if (!empty($post['attachment'])) : ?> <?php if (!variable_get('fbstream_block_post_display_more', 1)) : ?>
<div class="more_link"><a href="#"
	post_id="<?php echo $post['post_id']; ?> ">more</a></div>
<?php $classname = 'hide'; ?> <?php else: $classname = ''; endif; ?>
<div class="more_info <?php echo $classname; ?> "
	post_id="<?php echo $post['post_id']; ?> "><?php if (!empty($post['attachment']['media'])) : ?>
<div class="media_wrapper"><?php foreach ($post['attachment']['media'] as $media) : ?>
<a href="<?php echo isset($media['href'])?$media['href']:'#'; ?> "><img
	src="<?php echo $media['src']; ?> " /></a> <?php endforeach; ?></div>
<?php endif; ?>

<div class="attachments_info"><?php if (array_key_exists('name', $post['attachment'])) : ?>
<div class="att_name"><a
	href="<?php echo @$post['attachment']['media'][0]['href']; ?> "><?php echo $post['attachment']['name']; ?></a></div>
<?php endif; ?> <?php if (array_key_exists('caption', $post['attachment'])) : ?>
<div class="att_caption"><?php echo $post['attachment']['caption']; ?></div>
<?php endif; ?> <?php if (array_key_exists('description', $post['attachment'])) : ?>
<div class="att_desc"><?php echo $post['attachment']['description']; ?></div>
<?php endif; ?></div>
</div>
<?php endif; ?>
<div style="clear: both;"></div>
<div class="time_comments_likes"><em> <span class="like_link"> <?php if (isset($post['likes']['user_likes']) && $post['likes']['user_likes'] != '') :?>
<a
	href="<?php echo isset($post['likes']['count'])?$post['likes']['count']:0;?>"
	title="<?php echo $post['post_id']; ?>" name='Unlike'>Unlike</a></span>
&bull; <?php else: ?> <a
	href="<?php echo isset($post['likes']['count'])?$post['likes']['count']:0;?>"
	title="<?php echo $post['post_id']; ?>" name='Like'>Like</a></span>
&bull; <?php endif;?> <?php $time_duration = time_duration(time() - $post['created_time']).' ago'; ?>
<span class="time"><?php echo $time_duration; ?></span> </em></div>
<div class="fb_stream_post_comments"
	id="fb_stream_post_comments_<?php echo $post['post_id'];?>"><?php if($post['comments']['count'] > 0) :
	$comments = $post['comments']['comment_list'];
	?> <?php
	foreach ($comments as $comment) {
	  $user_info = call_facebook_api('users.getInfo', array('uids' => $comment['fromid'], 'fields' => 'name,pic_square,profile_url'));
	  $user_info = @$user_info[0];
	  ?>
<div
	class="fb_post_comment <?php echo $string = str_replace (" ", "",$comment['id']) ?> ">
<div class="fb_comment_user_picture"><img
	src="<?php echo $user_info['pic_square']; ?>" /></div>
<div class="fb_comment_message"><a
	href="<?php echo $user_info['profile_url']; ?> "><?php echo $user_info['name'] ?>
</a> <span class="fb_comment_user_message"><?php echo str_parse_url(check_plain($comment['text'])); ?>
</span> <?php $time = time_duration(time() - $comment['time']).' ago'; ?>
<div class="fb_comment_created"><?php echo $time ?></div>
</div>
<div style="clear: both;"></div>
</div>
<?php  } ?> <?php endif;?></div>
<div class="fb_post_comment"><?php $comment_count = 0; if ($post['comments']['count'] > 0) : ?>
<?php $comment_count = $post['comments']['count']; ?> <?php endif;?>
<div class="post_comments_form"><span class="user_like"
	id="user_like_<?php echo $post['post_id'];?>"> <?php if (isset($post['likes']['user_likes']) && $post['likes']['user_likes'] != '') :?>
You <?php if ($post['likes']['count'] > 0) : ?>and other <?php endif;?>
<?php endif;?> </span> <span class="likes"
	id="likes_<?php echo $post['post_id'];?>"> <?php
	$count = isset($post['likes']['count']) ? $post['likes']['count'] : '';
	$post_id = trim($post['post_id']);
	if (isset($post['likes']['count']) && $post['likes']['count'] > 0) :
	$likes_str = ($post['likes']['count'] == 1) ? 'like':'likes';
	$likes =  $post['likes']['count'];
	if(isset($post['likes']['user_likes']) && $post['likes']['user_likes'] != '') :
	$likes = $likes - 1;
	endif;
	?> <?php echo $likes. ' ' . $likes_str; ?> &bull; <?php
	endif;
	?> </span> <span class="comments"
	id="comments_<?php echo $post['post_id'];?>"> <?php if ($post['comments']['count'] > 0) : ?>
	<?php $comments_str = ($post['comments']['count'] == 1) ? 'comment':'comments'; ?>
	<?php $comments_str = $post['comments']['count'] .' '. $comments_str; ?>
	<?php echo $comments_str; ?> <?php endif; ?> </span>
<div id="loader_<?php echo $post['post_id'];?>" style="display: none;"></div>
	<?php echo drupal_render(drupal_get_form('post_comment', $post['post_id'], $comment_count)); ?>
</div>
</div>
</div>
<div style="clear: both;"></div>

