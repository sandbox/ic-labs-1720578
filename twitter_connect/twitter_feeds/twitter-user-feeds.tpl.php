<?php
/**
 * @file
 * Template file for tweet feeds display
 */
$status == $variables['status'];

?>
<div id="twitter_container">
<?php if (isset($status['error'])) {
	echo $status['error'];
}
else {
?>	
<div class="twitterBox" id="twitterBox"><?php echo drupal_render(drupal_get_form('tweet_box')); ?>
</div>
<?php foreach ($status as $usertweet) {?>
<div class="tewwt_container"
	id="container_<?php echo $usertweet['id'];?>">
<div class="user_image"><a href="<?php echo $usertweet['user_url'];?>"
	target="_blank"><img src="<?php echo $usertweet['profile_image'];?>"
	class="twitter-img"></img></a></div>
<div class="tweet_des"><span class="tweet_user"> <a
	href="<?php echo $usertweet['user_url'];?>" target="_blank"><?php echo $usertweet['screen_name'];?></a>
</span> <span class="tweet_body"> <a
	href="<?php echo $usertweet['tweet_url'];?>" target="_blank"><?php echo $usertweet['text'];?></a>
</span> <?php if(isset($usertweet['in_reply']) && $usertweet['in_reply'] != '') :?>
<span class="tweet_reply"> <span class="reply_image">&nbsp;</span><em>In
reply to <?php echo $usertweet['in_reply'];?></em> </span> <?php endif; ?>
<span class="tweet_time"> <em><?php echo $usertweet['time_ago'];?> via <?php echo $usertweet['source'];?></em>
&nbsp; &bull; <a href="#tweet_reply_box_<?php echo $usertweet['id'];?>"
	class="tweet_reply"
	title="Reply to @<?php echo $usertweet['screen_name'];?>" name="Reply">Reply</a>
<?php if (isset($usertweet['retweeted']) && $usertweet['retweeted'] == 1) :?>
&nbsp; &bull; <a href="<?php echo $usertweet['id'];?>"
	class="tweet_retweet" name="Retweeted">Retweeted</a> <?php else:?>
&nbsp; &bull; <a href="<?php echo $usertweet['id'];?>" name="Retweet">Retweet</a>
<?php endif;?> <?php if (isset($usertweet['favorited']) && $usertweet['favorited'] == 1) :?>
&nbsp; &bull; <a href="<?php echo $usertweet['id'];?>"
	class="tweet_favorited" name="Favorited" alt="Undo Favorite">Favorited</a>
<?php else:?> &nbsp; &bull; <a href="<?php echo $usertweet['id'];?>"
	name="Favorite">Favorite</a> <?php endif;?> </span></div>
</div>
<div style="display: none;">
<div id="tweet_reply_box_<?php echo $usertweet['id'];?>"
	class="tweet_reply_box">
<div class="tweet_form">
<div class="twitter-input"><textarea
	class="twitter-anywhere-tweet-box-editor"
	id="tweet_box_editor_<?php echo $usertweet['id'];?>">@<?php echo $usertweet['screen_name'];?></textarea>
</div>
<div class="twitter-button"><a href="#"
	name="<?php echo $usertweet['id'];?>"
	class="tweet-button btn primary-btn">Tweet</a></div>
</div>
<div id="original_post" class="tewwt_container inside-overlay">
<div class="user_image"><a href="<?php echo $usertweet['user_url'];?>"
	target="_blank"><img src="<?php echo $usertweet['profile_image'];?>"
	class="twitter-img"></img></a></div>
<div class="tweet_des"><span class="tweet_user"> <a
	href="<?php echo $usertweet['user_url'];?>" target="_blank"><?php echo $usertweet['screen_name'];?></a>
</span> <span class="tweet_body"> <a
	href="<?php echo $usertweet['tweet_url'];?>" target="_blank"><?php echo $usertweet['text'];?></a>
</span></div>
</div>
</div>
</div>
<?php 	}
}
?></div>
