/**
 *
 */
(function ($) {
  
  $(".tweet_time a.tweet_reply").fancybox({
	'titlePosition' 	: 'over',
	'transitionIn'		: 'none',
	'transitionOut'		: 'none'
  });
	
  Drupal.behaviors.twitter_post = {
    attach: function (context, settings) {
      $(".tweet_time a", context).click(function(event) {
        var type = $(this).attr('name');
        var tweet_id = $(this).attr('href');
        var link = $(this);
        switch(type) {
        	case 'Favorite': case 'Favorited':
        		$.post(Drupal.settings.basePath+'twitter/status/favorites',{id:tweet_id,optype:type},function(data){
        			var dataar = $.parseJSON(data);
        			if (dataar.error) {
        				alert(dataar.error);
        			} else {
	        			link.removeClass();    
	        			link.html(dataar.text);
	        			link.attr('name', dataar.text);
	        			link.addClass(dataar.cssclass);
        			}	
        		});
        		break;
        	case 'Retweet': 
        		$.post(Drupal.settings.basePath+'twitter/status/retweet',{id:tweet_id,optype:type},function(data){
        			var dataar = $.parseJSON(data);
        			if (dataar.error) {
        				alert(dataar.error);
        			} else {
	        			link.removeClass();    
	        			link.html(dataar.text);
	        			link.attr('name', dataar.text);
	        			link.addClass(dataar.cssclass);
        			}	
        		});
            	break;
        	case 'Retweeted':
        		alert(Drupal.t('Operation not possible, You have already retweeted this status.'));
        		break;
        	case 'Reply':
        		break;	
        	default: 
        		  alert(Drupal.t('There is an error while processing your request.'));
        		  break;	
        }        
        event.preventDefault();
        event.stopPropagation();
        return false;
      });
      
      $(".tweet_form a", context).click(function(event) {
    	  var id = $(this).attr('name');
    	  var msg = $("#tweet_box_editor_"+id).val();
    	  
    	  $.post(Drupal.settings.basePath+'twitter/status/reply',{id:id,messg:msg},function(data){
  			var dataar = $.parseJSON(data);
  			if (dataar.error) {
  				alert(dataar.error);
  			} else {
  				location.reload();
      			//$('#twitter_container').prepend(dataar.message);
      			$.fancybox.close();
  			}	
  		  });
      });
      
      $("#edit-tweet").keyup(function() {
    	  var len = this.value.length;
          if (len >= 140) {
        	  $('#tweetcount').text(140 - len);	
        	  $('#tweetcount').css('color','red');
          }else {
              $('#tweetcount').text(140 - len);
              $('#tweetcount').css('color','#C3C3C3');
          }
      });
      
      $(".tweet_add_form").submit(function(event) {
    	 // event.preventDefault();
    	//  event.stopPropagation();
    	  var tweet = this.tweet.value;
    	  if (tweet == '') {
    		  alert(Drupal.t("Please add comment..."));
    		  return false;
    	  } else if(tweet.length > 140) {
    		  alert(Drupal.t("Update should be less than 140 character."));
    		  return false;
    	  } else {
    		  return true;
    	  }
      });	  
    }
  };
}(jQuery));