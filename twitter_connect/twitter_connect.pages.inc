<?php

/**
 * @todo Please document this function.
 * @file Contains admin form
 */
function twitter_connect_user_settings($account) {
module_load_include('inc', 'twitter_connect');
  $output = array();
  if (!empty($account->twitter_accounts)) {
    $output['list_form'] = drupal_get_form('twitter_connect_account_list_form', $account->twitter_accounts);
  } else {
    $output['form'] = drupal_get_form('twitter_connect_account_form', $account);
  }  

  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function twitter_connect_account_list_form($form, $form_state, $twitter_accounts = array()) {
  $form['#tree'] = TRUE;
  $form['accounts'] = array();

  foreach ($twitter_accounts as $twitter_account) {
    $form['accounts'][] = _twitter_account_list_row($twitter_account);
  }

  if (!empty($twitter_accounts)) {
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
  }

  return $form;
}

function _twitter_account_list_row($account) {
  $form['#account'] = $account;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $account->id,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['screen_name'] = array(
    '#type' => 'value',
    '#value' => $account->screen_name,
  );

  $form['image'] = array(
    '#markup' => theme('image', array('path' => $account->profile_image_url)),
  );

  $form['visible_name'] = array(
    '#markup' => l($account->screen_name, 'http://www.twitter.com/' . $account->screen_name),
  );

  $form['description'] = array(
    '#markup' => filter_xss($account->description),
  );

  $form['protected'] = array(
    '#markup' => empty($account->protected) ? t('No') : t('Yes'),
  );

  $form['delete'] = array(
    '#type' => 'checkbox',
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function theme_twitter_connect_account_list_form($variables) {
  $form = $variables['form'];

  $header = array('', t('Name'), t('Description'), t('Private'), t('Delete'));

  $rows = array();

  foreach (element_children($form['accounts']) as $key) {
    $element = &$form['accounts'][$key];
    $row = array(
      drupal_render($element['image']),
      drupal_render($element['id']) . drupal_render($element['screen_name']) . drupal_render($element['visible_name']),
      drupal_render($element['description']),
      drupal_render($element['protected']),
      drupal_render($element['delete']),
      );

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function twitter_connect_account_list_form_submit($form, &$form_state) {
  $accounts = $form_state['values']['accounts'];
  foreach ($accounts as $account) {
    if (empty($account['delete'])) {
      twitter_account_save($account);
      drupal_set_message(t('The Twitter account settings were updated.'));
    }
    else {
      twitter_account_delete($account['id']);
      drupal_set_message(t('The Twitter account was deleted.'));
    }
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function twitter_connect_user_make_global($form, $form_state, $account, $twitter_uid) {
  module_load_include('inc', 'twitter_connect');

  $twitter_account = twitter_account_load($twitter_uid);

  $form = array();

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['twitter_uid'] = array(
    '#type' => 'value',
    '#value' => $twitter_uid,
  );

  if ($twitter_account->is_global) {
    $text = t('Are you sure you want to remove %screen_name from the global accounts?', array('%screen_name' => $twitter_account->screen_name));
    $description = t('This means other users will no longer be allowed to post using this account.');
  }
  else {
    $text = t('Are you sure you want to allow other users to access the %screen_name account?', array('%screen_name' => $twitter_account->screen_name));
    $description = t('This will allow other users to post using this account.');
  }

  return confirm_form($form, $text, 'user/' . $account->uid . '/edit/twitter', $description);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function twitter_connect_user_make_global_submit($form, &$form_state) {
  db_update('twitter_account')
  ->expression('is_global', '(1 - is_global)')
  ->condition('twitter_uid', $form_state['values']['twitter_uid'])
  ->execute();

  $form_state['redirect'] = 'user/' . $form_state['values']['uid'] . '/edit/twitter';
}

/**
 * Form to add a Twitter account
 *
 * If OAuth is not enabled, a text field lets users to add their
 * Twitter screen name. If it is, a submit button redirects to
 * Twitter.com asking for authorisation.
 */
function twitter_connect_account_form($form, $form_state, $account = NULL) {
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  if (_twitter_use_oauth()) {
    $form['#validate'] = array('twitter_account_oauth_validate');
  }
  else {
    $form['screen_name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Twitter user name'),
    );

    $form['import'] = array(
      '#type' => 'checkbox',
      '#title' => t('Import statuses from this account'),
      '#default_value' => TRUE,
      '#access' => FALSE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add account'),
  );

  return $form;
}

/**
 * Implements hook_FORM_ID_submit()
 *
 * Loads Twitter account details and adds them to the user account
 */
function twitter_connect_account_form_submit($form, &$form_state) {
  module_load_include('lib.php', 'twitter_connect');
  module_load_include('inc', 'twitter_connect');

  $name = $form_state['values']['screen_name'];
  $twitter = new Twitter($name);
  $account = $twitter->users_show($name, FALSE);
  twitter_account_save($account, TRUE, user_load($form_state['values']['uid']));
}

/**
 * If OAuth is enabled, intercept submission of 'Add Account' form on
 * user/%/edit/twitter page and redirect to Twitter for auth.
 */
function twitter_account_oauth_validate($form, &$form_state) {
  module_load_include('lib.php', 'oauth_common');
  module_load_include('lib.php', 'twitter_connect');

  $key = variable_get('twitter_consumer_key', '');
  $secret = variable_get('twitter_consumer_secret', '');
  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your Twitter consumer key and secret.'));
  }

  $twitter = new TwitterOAuth($key, $secret);
  $token = $twitter->get_request_token();

  $_SESSION['twitter_oauth']['account'] = user_load($form['uid']['#value']);
  $_SESSION['twitter_oauth']['token'] = $token;
  $_SESSION['twitter_oauth']['destination'] = $_GET['q'];
  drupal_goto($twitter->get_authorize_url($token));
}

/**
 * @TODO This code should probably be reviewed.
 *
 * Wrapper to call drupal_form_submit() which wasn't required in D6.
 */
function twitter_connect_oauth_callback() {
  $form_state['values']['oauth_token'] = $_GET['oauth_token'];
  drupal_form_submit('twitter_connect_oauth_callback_form', $form_state);
}

/**
 * Form builder function. In D6 this form was built in response to the
 * oauth return request from Twitter, and the setting of
 * $form['#post'] seems to have caused the form to be validated and
 * processed.
 */
function twitter_connect_oauth_callback_form($form, &$form_state) {
  $form['#post']['oauth_token'] = $_GET['oauth_token'];
  $form['oauth_token'] = array(
    '#type' => 'hidden',
    '#default_value' => $_GET['oauth_token'],
  );
  return $form;
}

/**
 * Validate results from Twitter OAuth return request.
 */
function twitter_connect_oauth_callback_form_validate($form, &$form_state) {
  $key = variable_get('twitter_consumer_key', '');
  $secret = variable_get('twitter_consumer_secret', '');

  if ($key == '' || $secret == '') {
    form_set_error('', t('Please configure your Twitter consumer key and secret.'));
  }

  if (isset($_SESSION['twitter_oauth'])) {
    $form_state['twitter_oauth'] = $_SESSION['twitter_oauth'];
    unset($_SESSION['twitter_oauth']);
  }
  else {
    form_set_error('oauth_token', 'Invalid Twitter OAuth request');
  }

  if (isset($form_state['twitter_oauth']['token'])) {
    $token = $form_state['twitter_oauth']['token'];
    if (!is_array($token) || !$key || !$secret) {
      form_set_error('oauth_token', t('Invalid Twitter OAuth request'));
    }
    if ($token['oauth_token'] != $form_state['values']['oauth_token']) {
      form_set_error('oauth_token', t('Invalid OAuth token.'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Twitter OAuth request'));
  }

  module_load_include('lib.php', 'oauth_common');
  module_load_include('lib.php', 'twitter_connect');
  module_load_include('inc', 'twitter_connect');

  if ($twitter = new TwitterOAuth($key, $secret, $token['oauth_token'], $token['oauth_token_secret'])) {
    if ($response = $twitter->get_access_token()) {
      $form_state['twitter_oauth']['response'] = $response;
    }
    else {
      form_set_error('oauth_token', t('Invalid Twitter OAuth request'));
    }
  }
  else {
    form_set_error('oauth_token', t('Invalid Twitter OAuth request'));
  }
}

/**
 * Handle a Twitter OAuth return request and store the account creds
 * in the DB. Redirects to user/%/edit/twitter
 *
 * @TODO Redirect better.
 *
 * I don't much like the use of drupal_goto() here as it might
 * interfere with other modules trying to piggyback on the form
 * submission, but setting $form['redirect'] was leaving us at the
 * twitter/oauth URL.
 */
function twitter_connect_oauth_callback_form_submit(&$form, &$form_state) {
  $key = variable_get('twitter_consumer_key', '');
  $secret = variable_get('twitter_consumer_secret', '');
  $response = $form_state['twitter_oauth']['response'];

  $twitter = new TwitterOAuth($key, $secret, $response['oauth_token'], $response['oauth_token_secret']);
  $twitter_account = $twitter->users_show($response['screen_name']);
  $twitter_account->set_auth($response);
  $account = $form_state['twitter_oauth']['account'];
  twitter_account_save($twitter_account, TRUE, $account);

  $form['#programmed'] = FALSE;

  $form_state['redirect'] = url('user/' . $account->uid . '/edit/twitter');
  // redirect isn't firing - because we're using drupal_submit_form()?
  // - so adding drupal_goto() here (but not liking it).
  if (isset($form_state['twitter_oauth']['login-flag']) && $form_state['twitter_oauth']['login-flag'] == 1) {
    drupal_goto('twitter/feeds');
  }
  else {
    drupal_goto('user/' . $account->uid . '/edit/twitter');
  }
}
