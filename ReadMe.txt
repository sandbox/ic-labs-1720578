﻿-- SUMMARY --

Social space module provides an integrated view of user’s activities on various social networks using single sign-on. The current version of the module supports Google, Facebook, Twitter, Foursquare and LinkedIn accounts.
This module enables the user to register and then login with any of the above listed social networks. Logged in users can consume the data feeds from social networks as well as post the data back. 

-- Login Buttons Block --

When Social space module is enabled, please move the "Sign in with Social Space" block to appropriate section. This block lists login buttons for social sites. 
 
-- Social Account --

It provides options for the user to add other accounts with their single account. It provides configuration options in User profile edit page for this. User can connect their individual account with Drupal user. It also provides option to remove linked accounts from list.

-- Social sites Feeds –

Based on permission set in admin section user can access there site feeds in Drupal. Additionally they can post back updates to the social site.
Facebook - It enables the user to list Facebook activity feeds in Drupal. Additionally user can post status update to the site and can like or comment on any post.

Twitter – It enables the user to import all home timeline feeds. Additionally user can send tweets back to twitter.
Foursquare – It imports user check-ins and comments. Additionally it enables the  user to post comments on any check-in
Google – It enables the user to import Google+ feeds.
YouTube- It imports all user activity feeds. Additionally user can upload a video to YouTube from this module.
LinkedIn- It imports all the LinkedIn activity feeds for user. Additionally the user can post back to LinkedIn. 

-- Head JS support –

This module provides option for user to use the Head.js support. This js library helps to load all site JavaScript in parallel resulting in improvement in the page performance and page loading time
-- REQUIREMENTS --

PHP 5.2 or higher versions.
Drupal 7.x.
Facebook PHP 3.x Library or higher: http://github.com/facebook/php-sdk/ (OAuth 2.0 Support Now)
Facebook API key: http://www.facebook.com/developers/
Google PHP client http://google-api-php-client.googlecode.com/files/google-api-php-client-0.4.7.tar.gz or copy folder from “lib” folder provided with this module.
Google API Key: https://code.google.com/apis/console/
Foursquare API key: http://www.foursquare.com/oauth
LinkedIn API Key: https://www.linkedin.com/secure/developer
Twitter API Key: https://dev.twitter.com/apps/new

-- INSTALLATION –-

1. Upload the ‘libraries’ and ‘oauth’ module to the module directory and activate this 2 modules.
2. Register the applications with each social site as API key for each application needed to be added in module configuration option.
3. Upload “facebook-php-sdk” and “google-api-php-client” (rename the downloaded folder to “google-api-php-client” if it’s not same).
4. Enable all listed modules under “Social” package. 

-- Setting up module --

1. Go to the “/admin/config/services/social” and add API keys for all sites. 
2. For head.js support please select the checkbox and click on save.
3. It also provides option for look and feel for login buttons.
4. Setup the permission as per the user role. Module provides options for admin to setup a access permission.
5. Admin need to assign the permission to import the feeds, if proper permission are not set user will not be able to view there feeds.

Note: If google API not works properly, please make sure valid certificate is added in "google-api-php-client" folder. We have added one in "lib\google-api-php-client" folder.

-- Know Issues --

1. User deletes the user account before un-linking the account with the social media to which he is associated. In this scenario an error message is shown when the user tries to register again with the same ID. Reason: When user account is deleted, there needs to be a support for unlinking the additional accounts. This will be handled in future.
2. For Facebook connect for user who is not logged in it requires steps to login. First user needs to connect to site using there Facebook account and when done it gives option for user login to site.
3. Currently all JavaScript works only if head.js support is enabled. To facilitate this option “to disable this” this support is removed. This will be added back when the issue mentioned here is fixed.
