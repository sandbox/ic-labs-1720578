Google API PHP Client (v.0.4.7)
==========================

http://code.google.com/p/google-api-php-client/

@author Chris Chabot <chabotc@google.com>
@author Chirag Shah <chirags@google.com>


Usage
-----

$client = new apiClient();
$client->setApplicationName("Google+ PHP Starter Application");
Visit https://code.google.com/apis/console to generate your
oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
$client->setClientId('insert_your_oauth2_client_id');
$client->setClientSecret('insert_your_oauth2_client_secret');
$client->setRedirectUri('insert_your_oauth2_redirect_uri');
$client->setDeveloperKey('insert_your_developer_key');
$client->setScopes(array('https://www.googleapis.com/auth/plus.me'));
$plus = new apiPlusService($client);

Tests
-----


    
Report Issues/Bugs
===============


