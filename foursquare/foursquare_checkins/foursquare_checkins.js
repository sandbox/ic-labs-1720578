Drupal.foursquare = Drupal.foursquare || {};
Drupal.foursquare.comment_form_submit = function(event){
	event.preventDefault();
    event.stopPropagation();
    
    var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
    var comment = this.comment.value; 
    var checkin_id = this.checkin_id.value;
    
    var form = this;
    if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	return false;
    }
    jQuery.ajax({
    	url: Drupal.settings.basePath+'foursquare/post/comments',
    	type: "POST",
    	data: {id:checkin_id,comments:comment},
    	beforeSend: function() {
    		jQuery('#loader_'+checkin_id).show();
    		jQuery('#loader_'+checkin_id).append(loaderImg);
    	},
    	success: function(data) {
    		form.comment.value = '';
    		jQuery(data).appendTo(jQuery('#fs_stream_post_comments_'+checkin_id));
    		jQuery('#loader_'+checkin_id).hide();
    		jQuery('.loader').remove();
    	}
    });
}
jQuery('.fs_comment_add_form').bind('submit', Drupal.foursquare.comment_form_submit);