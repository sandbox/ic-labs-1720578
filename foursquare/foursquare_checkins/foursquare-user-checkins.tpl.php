<?php

/**
 * @file
 * Template file for Foursquare items as displayed on user page
 *
 */
$checkins == $variables['checkins'];
?>
<div class="foursquare-checkins"><?php
if(isset($checkins['error-code']) && $checkins['error-code'] != '') {
  echo $checkins['message'];
} else {
  foreach ($checkins as $checkin) {
    ?>
<div class="foursquare-checkin">
<div class="venue-thumb"><img
	src="<?php echo $checkin['checkin_venue_thumb'];?>" /></div>
<div class="venue-name"><a
	href="https://foursquare.com/v/<?php echo $checkin['checkin_venue_name'];?>/<?php echo $checkin['checkin_venue_id'];?>"
	target="_blank"><?php echo $checkin['checkin_venue_name'];?></a></div>
<div class="checkin-time"><a
	href="https://foursquare.com/user/<?php echo $checkin['user_id'];?>/checkin/<?php echo $checkin['checkin_id'];?>"
	target="_blank"><?php echo $checkin['checkin_timestamp'];?></a></div>
<div class="clear"></div>
<div class="fs_stream_post_comments"
	id="fs_stream_post_comments_<?php echo $checkin['checkin_id'];?>"><?php
	if(isset($checkin['comments']) && count($checkin['comments']) > 0) :
	foreach($checkin['comments'] as $comment) {
	  ?>
<div class="fs_post_comment <?php echo $comment->id; ?> ">
<div class="fs_comment_user_picture"><img
	src="<?php echo $comment->user->photo; ?>" height="35" width="35" /></div>
<div class="fs_comment_message"><span><a
	href="<?php echo isset($comment->user->canonicalUrl)? $comment->user->canonicalUrl: "https://foursquare.com/user/" . $comment->user->id;?> "><?php echo $comment->user->firstName.' '.$comment->user->lastName; ?>
</a></span>&nbsp;<span class="fs_comment_created"><?php echo time_duration(time() - $comment->createdAt).' ago'; ?></span>
<div class="fs_comment_user_message"><?php echo str_parse_url(check_plain($comment->text)); ?>
</div>
</div>
<div style="clear: both;"></div>
</div>
	  <?php
	}
	endif;
	?></div>
<div class="fs_post_comment_form">
<div id="loader_<?php echo $checkin['checkin_id'];?>"
	style="display: none;"></div>
	<?php echo drupal_render(drupal_get_form('post_message_checkin', $checkin['checkin_id'])); ?>
</div>
</div>
	<?php
  }
}
?></div>
