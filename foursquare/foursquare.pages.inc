<?php
/**
 * @todo Please document this function.
 * @file Contains common function which are used by foursquare module.
 */
function foursquare_user_settings($account) {

  $output = array();
  $check = foursquare_get_account($account->uid);
  if (isset($check['error-code'])) {
    //$output .= t('You must first authorize Foursquare integration to use related features.');
    $output['form'] = drupal_get_form('foursquare_user_enable_form', $account->uid);
  }
  else {
    //$output .= t('Your account is associated with !name', array('!name' => l($check['firstName'] . ' ' . $check['lastName'], 'https://foursquare.com/user/'.$check['id'])));
    $output['form'] = drupal_get_form('foursquare_user_settings_form', array('account' => $account, 'fsaccount' => $check));
  }
  return $output;
}

/**
 * @todo Please document this function.
 */
function foursquare_user_enable_form($form, $form_state, $uid) {
  $form['foursqaure-set'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('You must first authorize Foursquare integration to use related features.'),
  );
  $form['#user'] = $uid;
  $form['#action'] = url('foursquare/redirect');
  $form['foursqaure-set']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go Foursquare'),
    '#suffix' => '<p class="description">' . t('You will be taken to Foursquare website in order to complete the process.') . '</p>',
  );
  return $form;
}


/**
 * @todo Please document this function.
 */
function foursquare_user_settings_form($form, &$form_state, $accounts) {

  $form['foursquare-account'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('Your account is associated with !name', array('!name' => l($accounts['fsaccount']['firstName'] . " " . $accounts['fsaccount']['lastName'], 'https://foursquare.com/user/' . $accounts['fsaccount']['id']))),
  );
  // We will need the account at submit
  $form['#account'] = $accounts['account'];

  $form['foursquare-account']['foursquare']['reset'] = array(
    '#type' => 'submit',
    '#description' => t('Click here to unlink your Foursquare account.'),
    '#value' => t('Unlink'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 */
function foursquare_user_settings_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];

  if ($op == $form['foursquare-account']['foursquare']['reset']['#value']) {
    db_delete('foursquare_tokens')
    ->condition('uid', $form['#account']->uid)
    ->execute();
    drupal_set_message(t('Foursquare preferences have been reset'));
  }
}