<?php
/*
 * @file File to render all admin forms related to socail plugin
 */

function social_space($form, &$form_state) {
  $form['google_token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google application keys'),
    '#description' => t('Get your application keys at <a href="https://code.google.com/apis/console/">https://code.google.com/apis/console/</a>'),
  );
  $form['google_token']['google_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Client ID'),
    '#default_value' => variable_get('google_client_id', ''),
  );
  $form['google_token']['google_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Client Secret key'),
    '#default_value' => variable_get('google_client_secret', ''),
  );
  $form['google_token']['google_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#default_value' => variable_get('google_api_key', ''),
  );
  return system_settings_form($form);
}

/**
 * Google API settings form
 */
function social_google_admin_settings($form, &$form_state) {
  $form['google_token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google application keys'),
    '#description' => t('Get your application keys at <a href="https://code.google.com/apis/console/">https://code.google.com/apis/console/</a>'),
  );
  $form['google_token']['google_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Client ID'),
    '#default_value' => variable_get('google_client_id', ''),
  );
  $form['google_token']['google_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Client Secret key'),
    '#default_value' => variable_get('google_client_secret', ''),
  );
  $form['google_token']['google_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#default_value' => variable_get('google_api_key', ''),
  );
  return system_settings_form($form);
}
/*
 * Youtube API setting
 */
function social_admin_settings($form, &$form_state) {

  $form['social_js_setting'] = array(
     '#title' => t("Use Head.js setting to load Js file"),
     '#type' => 'checkbox',
     '#description' => t('Use this setting to load JS parallely.'),
     '#disabled' => TRUE, 
     '#default_value' => array(1),
     '#default_value' => variable_get('social_js_setting', '1')
  );
  return system_settings_form($form);

}

function social_youtube_admin_settings($form, &$form_state) {

  $form['youtube_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('YouTube API keys'),
    '#description' => t('Get your API keys at !link', array('!link' => l(t('YouTube Configuration'),"http://code.google.com/apis/youtube/dashboard/gwt/index.html"))),
  );
  $form['youtube_api']['youtube_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('YouTube API key'),
    '#default_value' => variable_get('youtube_api_key', ''),
  );
  return system_settings_form($form);

}

/*
 * Callback for admin settings at admin/settings/linkedin
 */

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function social_linkedin_admin_settings($form, &$form_state) {
  //LinkedIn API credentials
  global $base_url;
  $form['linkedin'] = array(
    '#title' => t('Base settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['linkedin']['linkedin_liboauth_path'] = array(
    '#default_value' => variable_get('linkedin_liboauth_path', ''),
    '#title' => t('OAuth library full path (including filename)'),
    '#type' => 'textfield',
    '#description' => t('Linkedin module needs the OAuth php library from http://code.google.com/p/oauth/. You can either download OAuth.php from there and specify the full path here or install the OAuth module from http://drupal.org/project/oauth (both 6.x-2 and 6.x-3 versions are supported)'),
  );

  $form['linkedin']['consumer'] = array(
    '#description' => t('You will get theses values by requesting an API key at https://www.linkedin.com/secure/developer. <br /><em>Notice that the "integration url" you specify at Linkedin must match the base url (@url)</em>', array('@url' => $base_url)),
    '#title' => t('Consumer information'),
    '#type' => 'fieldset',
  );
  $form['linkedin']['consumer']['linkedin_consumer_key'] = array(
    '#default_value' => variable_get('linkedin_consumer_key', ''),
    '#title' => t('API key'),
    '#type' => 'textfield',
  );
  $form['linkedin']['consumer']['linkedin_consumer_secret'] = array(
    '#default_value' => variable_get('linkedin_consumer_secret', ''),
    '#title' => t('API Secret key'),
    '#type' => 'textfield',
  );
  $form['linkedin']['linkedin_debug_mode'] = array(
    '#default_value' => variable_get('linkedin_debug_mode', 0),
    '#title' => t('Turn on debug mode'),
    '#type' => 'checkbox',
  );

  $form['linkedin']['linkedin_auth_bypass_register_checks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bypass user mail verification or administrator approval for user registering through LinkedIn'),
    '#default_value' => variable_get('linkedin_auth_bypass_register_checks', 0),
  );

  //Tabs on user page don't always get rebuild, so manually trigger it.
  $form['#submit'][] = 'menu_rebuild';
  return system_settings_form($form);
}

/**
 * Foursquare settings form
 */
function social_foursquare_admin_settings() {
  $form['foursquare_token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Foursquare application keys'),
    '#description' => t('Get your application keys at <a href="http://www.foursquare.com/oauth">http://www.foursquare.com/oauth</a>'),
  );
  $form['foursquare_token']['foursquare_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Foursquare Consumer Key'),
    '#default_value' => variable_get('foursquare_consumer_key', ''),
  );
  $form['foursquare_token']['foursquare_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Foursquare Secret Key'),
    '#default_value' => variable_get('foursquare_consumer_secret', ''),
  );
  return system_settings_form($form);
}

/**
 * Form builder; Twitter settings form.
 */
function social_twitter_admin_settings($form, &$form_state) {

  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth Settings'),
    '#access' => module_exists('oauth_common'),
    '#description' => t('To enable OAuth based access for twitter, you must <a href="@url">register your application</a> with Twitter and add the provided keys here.', array('@url' => 'https://dev.twitter.com/apps/new')),
  );
  $form['oauth']['callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#markup' => url('twitter/oauth', array('absolute' => TRUE)),
  );
  $form['oauth']['twitter_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer key'),
    '#default_value' => variable_get('twitter_consumer_key', NULL),
  );
  $form['oauth']['twitter_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('OAuth Consumer secret'),
    '#default_value' => variable_get('twitter_consumer_secret', NULL),
  );

  $form['sign-in'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sign In Settings'),
    '#description' => t('To Setup sign-in setting.'),
  );

  $img_path = drupal_get_path('module', 'social_space') . '/images/twitter';
  $results = file_scan_directory($img_path, '/.png/');

  $options = array();
  foreach ($results as $image) {
    $options[$image->filename] = theme('image', array('path' => $image->uri));
  }

  $form['sign-in']['twitter_signin_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select sign-in button'),
    '#options' => $options,
    '#default_value' => variable_get('twitter_signin_button', 'Sign-in-with-Twitter-lighter-small.png'),
  );

  $form['sign-in']['twitter_login_register'] = array(
    '#title' => t('Automatically register new users'),
    '#type' => 'radios',
    '#options' => array(t('No'), t('Yes')),
    '#default_value' => variable_get('twitter_login_register', 0),
    '#description' => t('Warning, if you enable this, new user accounts will be created without email addresses.'),
  );

  return system_settings_form($form);
}