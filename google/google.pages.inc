<?php
/**
 * @todo Please document this function.
 * @file Google API forms and common functions.
 */
function google_user_settings($account) {

  $output = array();
  $check = google_get_account($account->uid);
  if (isset($check['error-code'])) {
    $output['form'] = drupal_get_form('google_user_enable_form', $account->uid);
  }
  else {
    $output['form'] = drupal_get_form('google_user_settings_form', array('account' => $account, 'gaccount' => $check));
  }
  return $output;
}

/**
 * @todo Please document this function.
 */
function google_user_enable_form($form, $form_state, $uid) {

  $form['google-set'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('You must first authorize Google API integration to use related features.'),
  );
  $form['#user'] = $uid;
  $form['#action'] = url('google/redirect');
  $form['google-set']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Connect Google'),
    '#suffix' => '<p class="description">' . t('You will be taken to Google website in order to complete the process.') . '</p>',
  );
  return $form;
}


/**
 * @todo Please document this function.
 */
function google_user_settings_form($form, &$form_state, $accounts) {
  $form['google-account'] = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#description' => t('Your account is associated with !name', array('!name' => l(t($accounts['gaccount']['name']), $accounts['gaccount']['link']))),
  );
  // We will need the account at submit
  $form['#account'] = $accounts['account'];

  $form['google-account']['reset'] = array(
    '#type' => 'submit',
    '#description' => t('Click here to unlink your Google account.'),
    '#value' => t('Unlink'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 */
function google_user_settings_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];

  if ($op == $form['google-account']['reset']['#value']) {
    db_delete('google_tokens')
    ->condition('uid', $form['#account']->uid)
    ->execute();
    drupal_set_message(t('Google preferences have been reset'));
  }
}