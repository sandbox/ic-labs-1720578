<?php
/**
 * @file
 * Template file for Google Plus user activies to displayed on user page
 *
 */
$activities == $variables['activities'];
?>
<div class="foursquare-checkins"><?php
if(isset($activities['error-code']) && $activities['error-code'] != '') {
  echo $activities['message'];
} else {
  foreach ($activities as $activitiy) {
    ?>
<div class="plus-post">
<div class="user-image"><a
	href="<?php echo $activitiy['user']['url'];?>" target="_blank"><img
	src="<?php echo $activitiy['user']['image']['url'];?>"
	alt="<?php echo $activitiy['user']['displayName'];?>"></img></a></div>
<div class="post-inner">
<div class="post-info"><a href="<?php echo $activitiy['user']['url'];?>"
	target="_blank"><?php echo $activitiy['user']['displayName'];?></a>
&nbsp; - &nbsp; <span class="post-timestamp"><a
	href="<?php echo $activitiy['post_url'];?>" target="_blank"><?php echo $activitiy['published_time'];?></a></span>
</div>
<div class="post-content"><?php if(isset($activitiy['original_user']) && $activitiy['original_user']['displayName'] != '') { ?>
<div class="original-post"><span class="original-user"><img
	src="<?php echo $activitiy['original_user']['image']['url'];?>"
	width="24px" height="24px"
	alt="<?php echo $activitiy['original_user']['displayName'];?>"></img></span>
<span class="original-user-name"><a href="" target="_blank"><?php echo $activitiy['original_user']['displayName'];?></a>
originally shared this post:</span></div>
    <?php }?>
<div class="content-text"><?php echo $activitiy['post_content'];?></div>
<div class="attachement-wrapper">
<div class="attachement-title"><a
	href="<?php echo $activitiy['attachments']['url'];?>" target="_blank"><?php echo $activitiy['attachments']['displayName'];?></a>
</div>
    <?php if(isset($activitiy['attachments']) && isset($activitiy['attachments']['att_image'])) {?>
<div class="attachement-image"><a
	href="<?php echo $activitiy['attachments']['url'];?>" target="_blank">
<img src="<?php echo $activitiy['attachments']['att_image'];?>"
	alt="<?php echo $activitiy['attachments']['displayName']?>"></img> </a>
</div>
    <?php }?>
<div class="attachement-content"><?php echo $activitiy['attachments']['content'];?>
</div>
<div class="clear"></div>
</div>
</div>
<div class="post-share-plus"><span>Plusone (<?php echo $activitiy['plusoners_ct']?>)</span>
&nbsp;&nbsp; - &nbsp;&nbsp;<span>Comments (<?php echo $activitiy['replies_ct']?>)</span>
</div>
</div>
<div class="clear"></div>
</div>
    <?php
  }
}
?></div>
