<?php
// $id:
/**
 * @file
 * Module to integrate YouTube API with Drupal,
 * This module depend on Google API module
 * http://code.google.com/apis/youtube/dashboard/gwt/index.html
 */

/**
 * hook_init
 */
function google_youtube_init() {
  global $_gclient;

  if (function_exists('google_client')) {
    $_gclient = google_client();
  }
  else {
    drupal_set_message(t("Please install google api module."));
  }

  drupal_add_css(drupal_get_path('module', 'google_youtube') . '/google_youtube.css');
  drupal_add_js(drupal_get_path('module', 'google_youtube') . '/google_youtube.js');
}

/**
 * hook_menu
 */
function google_youtube_menu() {
  $items = array();

  $items['youtube/feeds'] = array(
      'title' => 'YouTube Activities',
      'type' => MENU_NORMAL_ITEM,
      'page callback' => '_get_activity_feeds',
      'page arguments' => array(1),
      'access arguments' => array('import youtube user activity feeds'),
  );

  $items['youtube/post/details'] = array(
      'title' => 'YouTube Video Upload',
      'type' => MENU_CALLBACK,
      'page callback' => '_video_details_upload',
      'access arguments' => array('access content'),
  );

  $items['youtube/post/comment'] = array(
      'title' => 'YouTube Video Comment',
      'type' => MENU_CALLBACK,
      'page callback' => 'post_video_comment',
      'access arguments' => array('access content'),
  );

  $items['youtube/post/rating'] = array(
      'title' => 'YouTube Video Rating',
      'type' => MENU_CALLBACK,
      'page callback' => 'post_video_rating',
      'access arguments' => array('access content'),
  );

  return $items ;
}

/**
 * Implements hook_permission().
 */
function google_youtube_permission() {
  return array(
    'import youtube user activity feeds' => array(
      'title' => t('Import YouTube user activity feeds.'),
  ),
  );
}

/*
 * Get the activity feeds of user
 * Thumbnail image for video
 * http://i1.ytimg.com/vi/_2ixdInlPzI/hqdefault.jpg
 */
function _get_activity_feeds() {
  global $_gclient;
  global $user;

  $result = db_query("SELECT * FROM {google_tokens} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAssoc();
  $arrStream = new stdClass;
  if (!$result || empty($result)) {
    $arrStream ->status = '401';
    $arrStream ->error_code = 'custom';
    $arrStream ->message = 'No Google account is associated with this user, please link your account using ' . l(t('Google Setting'), 'user/' . $user->uid . '/edit/google');
  }
  else {
    $arrStream = array();
    // get the YouTube API key
    $api_key = variable_get('youtube_api_key', NULL);
    if ($api_key == NULL) {
      drupal_set_message(check_plain(t("Please setup your YouTube API Key at !youtube", array('!youtube' => l(t("YouTube Setup"), 'admin/config/people/google/youtube')))));
    }
    // YouTube required different key, so setup YouTube key to API call.

    $_gclient->setDeveloperKey($api_key);
    $_gclient->setAccessToken($result['access_token']);

    if (isset($_GET['id']) && isset($_GET['status']) && $_GET['status'] == '200') {
      $custom_request = new apiHttpRequest("https://gdata.youtube.com/feeds/api/users/default/uploads/{$_GET['id']}?v=2&alt=json");
      $custom_request = apiClient::$auth->sign($custom_request);
      $custom_request = apiClient::$io->makeRequest($custom_request)->getResponseBody();
      $video_entry = json_decode($custom_request);
      $video_entry = (array) $video_entry->entry;
      $temp = array();
      $temp['user_info'] = $user->name;
      $temp['user_pic'] = theme("user_picture", array('account' => $user));
      $timestamp = (array) $video_entry['updated'];
      $temp['timestamp'] = nicetime($timestamp['$t']);

      $temp['video_title'] =  (array) $video_entry['title'];
      $temp['video_link'] =  $video_entry['link']['0']->href;
      $temp['video_author'] =  isset($video_entry['author']['0']->name)? (array)$video_entry['author']['0']->name:'';
      // Convert stdobject to array
      $video_media = (array) $video_entry['media$group'];
      $comment_request_up = (array)$video_entry['gd$comments'];
      $comment_request_link_up = $comment_request_up['gd$feedLink']->href;
      $temp['post_link'] = $comment_request_link_up;
      $temp['video_desc'] = (array) $video_media['media$description'];
      $temp['video_id']['$t'] =  $_GET['id'];
      $temp['user_action'] = 'uploaded';
      $temp['video_views'] =  isset($video_entry['yt$statistics']->viewCount)?number_format($video_entry['yt$statistics']->viewCount):0;
      $arrStream[] = $temp;
    }
    // Call user activity feed API.
    $request = new apiHttpRequest("https://gdata.youtube.com/feeds/api/users/default/events?v=2&alt=json&inline=true");

    $request = apiClient::$auth->sign($request);
    $request = apiClient::$io->makeRequest($request)->getResponseBody();

    $userFeeds = json_decode($request);

    if (isset($userFeeds->feed) && count($userFeeds->feed->entry) > 0 ) {
      foreach ($userFeeds->feed->entry as $entry) {
        $temp= array();
        $entry = (array) $entry;
        $temp['user_info'] = $user->name;
        $temp['user_pic'] = theme("user_picture", array('account' => $user));
        $timestamp = (array) $entry['updated'];
        $temp['timestamp'] = nicetime($timestamp['$t']);

        // Get video details
        if (isset($entry['link']['1']->entry)) {
          $video_details = (array) $entry['link']['1']->entry['0'];
          $temp['video_title'] =  (array) $video_details['title'];
          $temp['video_link'] =  $video_details['link']['0']->href;
          $temp['video_author'] =  isset($video_details['author']['0']->name)? (array)$video_details['author']['0']->name:'';

          if (isset($video_details['gd$comments']) && count($video_details['gd$comments']) > 0) {
            $comment_request = (array)$video_details['gd$comments'];
            $comment_request_link = $comment_request['gd$feedLink']->href;
            $request_comment = new apiHttpRequest($comment_request_link . "&alt=json");
            $request_comment = apiClient::$io->makeRequest($request_comment)->getResponseBody();
            $video_comment = json_decode($request_comment);
            if (isset($video_comment->feed->entry) && count($video_comment->feed->entry) > 0) {
              $comment_entry = (array) $video_comment->feed->entry;
              if (count($comment_entry) > 5) {
                $temp['video_comment'] = array_slice($comment_entry, 0, 5);
                $temp['video_comment_more'] = 1;
              }
              else {
                $temp['video_comment'] = $comment_entry;
              }
            }
          }

          $temp['video_likes'] = isset($video_details['yt$rating']->numLikes)?$video_details['yt$rating']->numLikes:0;
          $temp['video_dislikes'] = isset($video_details['yt$rating']->numDislikes)?$video_details['yt$rating']->numDislikes:0;
          $temp['video_fav'] = isset($video_details['yt$statistics']->favoriteCount)?$video_details['yt$statistics']->favoriteCount:0;
          // Convert stdobject to array
          $video_media = (array) $video_details['media$group'];
          $temp['video_desc'] = (array) $video_media['media$description'];
          $temp['video_id'] =  (array) $video_details['id'];
          $temp['post_link'] = $comment_request_link;
          $temp['video_views'] =  isset($video_details['yt$statistics']->viewCount)?number_format($video_details['yt$statistics']->viewCount):0;
        }

        // Get user action
        switch ($entry['category']['1']->term) {
          case 'video_rated':
            $temp['user_action'] = 'liked';
            break;
          case 'video_shared':
            $temp['user_action'] = 'shared';
            break;
          case 'video_favorited':
            $temp['user_action'] = 'favorited';
            break;
          case 'video_commented':
            $temp['user_action'] = 'commented';
            break;
          case 'video_uploaded':
            $temp['user_action'] = 'uploaded';
            break;
          default:
            $temp['user_action'] = 'liked';
            break;
        }
        $arrStream[] = $temp;
      }
    } 
    else {
      $arrStream ->status = '403';
      $arrStream ->error_code = 'custom';
      $arrStream ->message = 'User Feeds are not available';
    }
  }
  return theme('youtube-user-activites', array('activities' => $arrStream));
}

/**
 * Implementation of hook_theme().
 */
function google_youtube_theme() {
  return array(
    'youtube-user-activites' => array(
      'template' => 'youtube-user-activites',
      'arguments' => array('activities' => NULL),
  ),
  );
}

/*
 * Post video
 */
function google_youtube_post_video($form, &$form_status) {

  $form['ytpost'] = array(
    '#title' => t('Upload video'),
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
  );

  $form['ytpost']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 92,
    '#description' => t("")      
  );

  $form['ytpost']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#cols' => 40,
    '#description' => t("")      
  );

  $option = array('0' => '-- Select a category --', 'Autos' => 'Autos & Vehicles', 'Comedy' => 'Comedy', 'Education' => 'Education', 'Entertainment' => 'Entertainment', 'Film' => 'Film & Animation', 'Howto' => 'Howto & Style', 'Music' => 'Music', 'News' => 'News & Politics', 'People' => 'People & Blogs', 'Sports' => 'Sports', 'Travel' => 'Travel & Events');

  $form['ytpost']['category'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#default_value' => '-- Select a category --',
    '#options' => $option      
  );
  $form['ytpost']['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Tags'),
    '#size' => 92,
    '#description' => t("Add comma seprated list")      
  );

  $form['ytpost']['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );

  $form['#attributes'] = array('class' => 'youtube_video_add_form');
  return $form;
}
/*
 * Post a comment to video
 */
function google_youtube_post_comment($form, &$form_status, $video_id, $post_link) {
  $form['ytpostcomment'] = array(
    '#title' => t('Post a Comment'),
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
  );

  $form['ytpostcomment']['comment'] = array(
    '#type' => 'textarea',
    '#title' => t(''),
    '#cols' => 40,
    '#rows' => 2,
    '#description' => t("")      
  );

  $form['video_id'] = array(
    '#type' => 'hidden',
    '#value' => $video_id,
  );

  $form['post_link'] = array(
    '#type' => 'hidden',
    '#value' => $post_link,
  );

  $form['ytpostcomment']['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Post'),
  );

  $form['#attributes'] = array('class' => 'youtube_comment_add_form');
  return $form;
}

/**
 * Uses youtube browser upload method to upload video.
 * @return json object with YouTube upload url and token with upload form.
 */
function _video_details_upload() {
  global $base_url;
  global $_gclient;
  global $user;

  $title = $_POST['title'];
  $desc = $_POST['desc'];
  $tags = $_POST['tags'];
  $category = $_POST['category'];

  $xml = '<?xml version="1.0"?>
              <entry xmlns="http://www.w3.org/2005/Atom"
                xmlns:media="http://search.yahoo.com/mrss/"
                xmlns:yt="http://gdata.youtube.com/schemas/2007">
                <media:group>
                  <media:title type="plain">' . $title . '</media:title>
                  <media:description type="plain">' . $desc . '</media:description>                  
                  <media:category
                    scheme="http://gdata.youtube.com/schemas/2007/categories.cat">' . $category . '
                  </media:category>
                  <media:keywords>' . $tags . '</media:keywords>
                </media:group>
              </entry>';

  $arrStream = new stdClass;
  $result = db_query("SELECT * FROM {google_tokens} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAssoc();
  if (!$result || empty($result)) {
    $arrStream->status = 'error';
    $arrStream->message = 'No Google account is associated with this user, please link your account using ' . l(t('Google Setting'), 'user/' . $user->uid . '/edit/google');
  }
  else {
    $api_key = variable_get('youtube_api_key', NULL);
    if ($api_key == NULL) {
      drupal_set_message(t("Please setup your YouTube API Key at") . l(t("YouTube Setup"), 'admin/config/people/google/youtube'));
    }
    // YouTube required different key, so setup YouTube key to API call.

    $_gclient->setDeveloperKey($api_key);
    $_gclient->setAccessToken($result['access_token']);
    $headers = array('GData-Version:2', 'X-GData-Key: key=' . trim($api_key), 'Content-length:' . trim(strlen($xml)), 'Content-Type:application/atom+xml; charset=UTF-8');
    // Call user activity feed API.
    $request = new apiHttpRequest("http://gdata.youtube.com/action/GetUploadToken", "POST", $headers, $xml);

    $request = apiClient::$auth->sign($request);
    $request = apiClient::$io->makeRequest($request)->getResponseBody();

    $response_token = simplexml_load_string($request);
    if (isset($response_token->url) && isset($response_token->token)) {
      // place to redirect user after upload
      $nextUrl = $base_url . '/youtube/feeds';
      // build the form
      $strhtml = '<legend><span class="fieldset-legend">Upload video</span></legend><form action="' . $response_token->url . '?nexturl=' . $nextUrl . '" method="post" enctype="multipart/form-data">' . '<input name="file" type="file"/>' . '<input name="token" type="hidden" value="' . $response_token->token . '"/>' . '<input value="Upload Video File" type="submit" />' . '</form>';
      $arrStream->status = 'sucess';
      $arrStream->message = $strhtml;
    }
    else {
      $arrStream->status = 'error';
      $arrStream->message = 'Error Uploading video details.';
    }

  }

  echo json_encode($arrStream);
  exit;
}
/**
 * Uses to post comment to Youtube.
 * @return JSON object
 */
function post_video_comment() {
  global $base_url;
  global $_gclient;
  global $user;

  $comment = $_POST['comment'];
  $video_id = $_POST['id'];
  $post_link = $_POST['link'];

  $arrStream = new stdClass;
  $result = db_query("SELECT * FROM {google_tokens} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAssoc();
  if (!$result || empty($result)) {
    $arrStream->status = 'error';
    $arrStream->message = 'No Google account is associated with this user, please link your account using ' . l(t('Google Setting'), 'user/' . $user->uid . '/edit/google');
  }
  else {
    $api_key = variable_get('youtube_api_key', NULL);
    if ($api_key == NULL) {
      drupal_set_message(t("Please setup your YouTube API Key at") . l(t("YouTube Setup"), 'admin/config/people/google/youtube'));
    }
    $_gclient->setDeveloperKey($api_key);
    $_gclient->setAccessToken($result['access_token']);

    $xml = '<?xml version="1.0" encoding="UTF-8"?>
					<entry xmlns="http://www.w3.org/2005/Atom"
    				xmlns:yt="http://gdata.youtube.com/schemas/2007">
  						<content>' . $comment . '</content>
					</entry>';
    $headers = array('GData-Version:2', 'X-GData-Key: key=' . trim($api_key), 'Content-length:' . trim(strlen($xml)), 'Content-Type:application/atom+xml; charset=UTF-8');

    // Call API.
    $request = new apiHttpRequest($post_link, "POST", $headers, $xml);

    $request = apiClient::$auth->sign($request);
    $request = apiClient::$io->makeRequest($request)->getResponseBody();

    $response_object = simplexml_load_string($request);

    $response = '<div class="video-comment" align="left">
						<div class="video-comment-msg">' . $response_object->content . '</div>
						<div class="video-data">
							<a href="http://www.youtube.com/user/' . $response_object->author->name . '" target="_blank">' . $response_object->author->name . '</a> &nbsp;&nbsp;
							' . nicetime($response_object->published) . '
						</div>
					</div>';
    $arrStream->status = 'sucess';
    $arrStream->message = $response;
  }
  echo json_encode($arrStream);
  exit;
}

/**
 * Post video rating to YouTube
 * @return JSON object
 */
function post_video_rating() {
  global $base_url;
  global $_gclient;
  global $user;

  $video_id = $_POST['id'];
  $clickvalue = $_POST['value'];
  // Get click value and decide which API we can call
  if ($clickvalue == 'like' || $clickvalue == 'dislike') {
    $xml_value = '<yt:rating value="' . $clickvalue . '"/>';
    $link = "https://gdata.youtube.com/feeds/api/videos/{$video_id}/ratings?v=2";
  } 
  elseif ($clickvalue == 'favorite') {
    $xml_value = '<id>' . $video_id . '</id>';
    $link = "https://gdata.youtube.com/feeds/api/users/default/favorites";
  }
  $arrStream = new stdClass;
  $result = db_query("SELECT * FROM {google_tokens} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAssoc();
  if (!$result || empty($result)) {
    $arrStream->status = 'error';
    $arrStream->message = 'No Google account is associated with this user, please link your account using ' . l(t('Google Setting'), 'user/' . $user->uid . '/edit/google');
  }
  else {
    $api_key = variable_get('youtube_api_key', NULL);
    if ($api_key == NULL) {
      drupal_set_message(t("Please setup your YouTube API Key at") . l(t("YouTube Setup"), 'admin/config/people/google/youtube'));
    }
    $_gclient->setDeveloperKey($api_key);
    $_gclient->setAccessToken($result['access_token']);

    $xml = '<?xml version="1.0" encoding="UTF-8"?>
					<entry xmlns="http://www.w3.org/2005/Atom"
    				xmlns:yt="http://gdata.youtube.com/schemas/2007">
  					' . $xml_value . '	
					</entry>';
    $headers = array('GData-Version:2', 'X-GData-Key: key=' . trim($api_key), 'Content-length:' . trim(strlen($xml)), 'Content-Type:application/atom+xml; charset=UTF-8');

    // Call API.
    $request = new apiHttpRequest($link, "POST", $headers, $xml);

    $request = apiClient::$auth->sign($request);
    $request = apiClient::$io->makeRequest($request)->getResponseBody();
    $response_object = simplexml_load_string($request);

    if (isset($response_object->error)) {
      $arrStream->status = 'error';
      $arrStream->message = (array) $response_object->error->internalReason;
    }
    else {
      $arrStream->status = 'success';
    }
  }
  echo json_encode($arrStream);
  exit;
}