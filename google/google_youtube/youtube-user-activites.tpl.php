<?php
/**
 * @file
 * Template file for Google Plus user activies to displayed on user page
 *
 */
$activities == $variables['activities'];
?>
<div class="youtube-feeds">
<?php
if(isset($activities->error_code) && $activities->error_code != '') {
echo $activities->message;
}
else {
 ?> 
<div class="yt_upload_video">
<div id="youtube_loader"></div>
<div id="youtube_initial_upload"><?php echo drupal_render(drupal_get_form('google_youtube_post_video')); ?></div>
<div id="youtube_upload_form"></div>
<div style="clear: both;"></div>
</div>
<?php
foreach ($activities as $activitiy) {
  ?>
<div class="video-container">
<div class="user-pic"><?php echo $activitiy['user_pic']; ?></div>
<div class="video-wrapper">
<div class="user-info"><span class="user-name"><?php echo $activitiy['user_info']; ?></span>
&nbsp; <?php echo $activitiy['user_action']; ?> &nbsp; <span
	class="activity-time"><a href="<?php echo $activitiy['video_link']; ?>"
	target="_blank"><?php echo $activitiy['timestamp']; ?></a></span></div>
<div class="video-details">
<div class="video-thumb"><a
	href="<?php echo $activitiy['video_link']; ?>" target="_blank"> <img
	src="http://i1.ytimg.com/vi/<?php echo $activitiy['video_id']['$t']; ?>/hqdefault.jpg"
	alt="<?php echo $activitiy['video_title']['$t']; ?>" width="160"
	height="120" /> </a></div>
<div class="video-meta">
<div class="video-title"><a
	href="<?php echo $activitiy['video_link']; ?>" target="_blank"> <?php echo $activitiy['video_title']['$t']; ?>
</a></div>
<div class="video-desc"><?php echo $activitiy['video_desc']['$t']; ?></div>
<div class="video-data"><a
	href="http://www.youtube.com/user/<?php echo $activitiy['video_author']['$t']; ?>"
	target="_blank"><?php echo $activitiy['video_author']['$t']; ?></a> <br />
  <?php echo $activitiy['video_views']; ?> views &nbsp;&nbsp; <span
	id="likes_<?php echo $activitiy['video_id']['$t'];?>"><?php echo isset($activitiy['video_likes'])?$activitiy['video_likes']:0;?>
<a href="like" id="<?php echo $activitiy['video_id']['$t'];?>"
	name="<?php echo isset($activitiy['video_likes'])?$activitiy['video_likes']:0;?>">likes</a></span>
&nbsp;&nbsp; <span
	id="dislikes_<?php echo $activitiy['video_id']['$t'];?>"><?php echo isset($activitiy['video_dislikes'])?$activitiy['video_dislikes']:0;?>
<a href="dislike" id="<?php echo $activitiy['video_id']['$t'];?>"
	name="<?php echo isset($activitiy['video_dislikes'])?$activitiy['video_dislikes']:0;?>">dislikes</a></span>
&nbsp;&nbsp; <span
	id="favorite_<?php echo $activitiy['video_id']['$t'];?>"><?php echo isset($activitiy['video_fav'])?$activitiy['video_fav']:0;?>
<a href="favorite" id="<?php echo $activitiy['video_id']['$t'];?>"
	name="<?php echo isset($activitiy['video_fav'])?$activitiy['video_fav']:0;?>">Favorite</a></span>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="youtube-comment-box post-comment-video">
<div
	id="youtube_comment_loader_<?php echo $activitiy['video_id']['$t'];?>"></div>
  <?php echo drupal_render(drupal_get_form('google_youtube_post_comment',$activitiy['video_id']['$t'],$activitiy['post_link'])); ?>
</div>
<div class="youtube-comment-box"
	id="youtube-comment-box-<?php echo $activitiy['video_id']['$t'];?>"
	align="center"><?php if (isset($activitiy['video_comment'])) :  ?> <?php	
	foreach ($activitiy['video_comment'] as $comments) {
	  ?>
<div class="video-comment" align="left">
<div class="video-comment-msg"><?php $message = (array) $comments->content; echo $message['$t']; ?></div>
<div class="video-data"><a
	href="http://www.youtube.com/user/<?php $user_name = (array) $comments->author[0]->name; echo $user_name['$t']; ?>"
	target="_blank"><?php echo $user_name['$t']; ?></a> &nbsp;&nbsp; <?php $timestamp_msg = (array) $comments->published; echo nicetime($timestamp_msg['$t'])?>
</div>
</div>
<div class="clear"></div>
	  <?php 		}
	  endif;
	  if (isset($activitiy['video_comment_more'])) :
	  ?>
<div class="show-more-link"><a
	href="http://www.youtube.com/all_comments?v=<?php echo $activitiy['video_id']['$t']?>"
	target="_blank">Read More</a></div>
	  <?php endif;?></div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
	  <?php
}
}
?></div>
