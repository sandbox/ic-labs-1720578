Drupal.youtube = Drupal.youtube || {};
Drupal.youtube.video_submit = function(event){
	
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
    var title = this.title.value;
    var description = this.description.value;
    var tags =  this.tags.value;
    var category = this.category.value;
   
    var form = this;
    if (title == '') {
    	alert(Drupal.t("Please add title..."));
    	return false;
    }
    if (category == 0) {
    	category = 'People';
    }
    
    jQuery.ajax({
    	url: Drupal.settings.basePath+'youtube/post/details',
    	type: "POST",
    	data: {title:title,desc:description,tags:tags,category:category},
    	beforeSend: function() {
    		jQuery('#youtube_loader').show();
    		jQuery('#youtube_loader').append(loaderImg);
    	},
    	success: function(data) {
    		var response = jQuery.parseJSON(data);
    		if (response.status == 'error') {
    			alert(response.message);
    			jQuery('#youtube_loader').hide();
	    		jQuery('.loader').remove();
    		} else {
	    		jQuery('#youtube_initial_upload').hide();
	    		jQuery('#youtube_upload_form').html(response.message);
	    		jQuery('#youtube_loader').hide();
	    		jQuery('.loader').remove();
    		}	
    	}
    });
    
    event.preventDefault();
    event.stopPropagation();
    return false;
}
Drupal.youtube.comment_submit = function(event){
	var loaderImg = '<img class="loader" src="/'+Drupal.settings.Modulepath+'/images/ajax-loader.gif" alt="" />';
    var comment = this.comment.value;
    var id = this.video_id.value;
    var link = this.post_link.value;
    
    var form = this;
    if (comment == '') {
    	alert(Drupal.t("Please add comment..."));
    	return false;
    }
    
    jQuery.ajax({
    	url: Drupal.settings.basePath+'youtube/post/comment',
    	type: "POST",
    	data: {comment:comment,id:id,link:link},
    	beforeSend: function() {
    		jQuery('#youtube_comment_loader_'+id).show();
    		jQuery('#youtube_comment_loader_'+id).append(loaderImg);
    	},
    	success: function(data) {
    		var response = jQuery.parseJSON(data);
    		if (response.status == 'error') {
    			alert(response.message);
    			jQuery('#youtube_comment_loader_'+id).hide();
	    		jQuery('.loader').remove();
    		} else {
    			form.comment.value = '';
    			jQuery(response.message).prependTo(jQuery('#youtube-comment-box-'+id));
	    		jQuery('#youtube_comment_loader_'+id).hide();
	    		jQuery('.loader').remove();
    		}	
    	}
    });
    
    event.preventDefault();
    event.stopPropagation();
    return false;
    
}
Drupal.youtube.link_click_handler = function(event){
	var video_id = jQuery(this).attr("id");
	var clickvalue = jQuery(this).attr("href");
	var ecount = jQuery(this).attr("name");
	var link = jQuery(this);
	
	jQuery.post(Drupal.settings.basePath+'youtube/post/rating',
	 {id:video_id,value:clickvalue},
	 function(data){
		 var response = jQuery.parseJSON(data);
		if (response.status != 'success') {
			alert(response.message);
		} else {
			location.reload();
		}
	});
	return false;
}
jQuery('.youtube_video_add_form').bind('submit', Drupal.youtube.video_submit);
jQuery('.youtube_comment_add_form').bind('submit', Drupal.youtube.comment_submit);
jQuery('.video-data span a').bind('click',Drupal.youtube.link_click_handler);